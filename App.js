import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default function App() {
  return (
    <View>
      <Text>😀</Text>
      <Text style={styles.hello}>{'Hola'}</Text>
      <Text style={styles.sinHello}>{'perrosko'}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  hello:{
    fontFamily: "NotoSansDisplay-SemiBold",
    fontSize: 30,
    textAlign: "center",
    margin: 10
  },
  sinHello:{
    fontSize: 30,
    textAlign: "center",
    margin: 10
  }
})
